# envconsul
FROM golang:1.14-stretch AS envconsul
ARG ENVCONSUL_VERSION=0.9.3
RUN curl -sf -LO https://github.com/hashicorp/envconsul/archive/v${ENVCONSUL_VERSION}.tar.gz \
    && tar -xvf v${ENVCONSUL_VERSION}.tar.gz \
    && cd envconsul-${ENVCONSUL_VERSION} \
    && make linux/amd64 \
    && mv pkg/linux_amd64/envconsul /usr/local/bin/envconsul

# use buildah for build
FROM ruby:2.5.7

RUN apt update \
    && apt install -y locales nodejs \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/* \
    && sed -i 's/# ru_RU.UTF-8 UTF-8/ru_RU.UTF-8 UTF-8/g' /etc/locale.gen \
    && sed -i 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/g' /etc/locale.gen \
    && locale-gen

ENV BUNDLER_VERSION=1.17.3
ENV LC_ALL=ru_RU.UTF-8
RUN gem install bundler -v '1.17.3'

WORKDIR /myapp
RUN mkdir log \
    && ln -sf /dev/null /myapp/log/production.log
COPY --from=envconsul /usr/local/bin/envconsul /usr/local/bin/envconsul
COPY Gemfile Gemfile.lock /myapp/
RUN bundle install

COPY . /myapp
ENTRYPOINT ["/usr/local/bin/envconsul"]

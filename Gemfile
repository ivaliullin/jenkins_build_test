source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.5.7'

gem 'rails', '~> 5.2.0'
gem 'pg'
gem 'puma', '~> 3.11'
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'
gem 'turbolinks', '~> 5'

gem 'jbuilder', '~> 2.5'

gem 'jwt', '~> 1.0'
gem 'dotenv-rails'
gem 'interactor', '~> 3.0'
gem 'sidekiq', '5.2.2'
gem 'smsc_ru'
gem 'redis'
gem 'phonelib'
gem 'json-schema'

gem 'rack-cors'

gem 'httparty'

gem 'active_model_serializers'

gem 'rails-i18n'

gem 'ssg_authorizer', git: 'git@bitbucket.org:sputniks/ssg_authorizer.git'
gem 'omniauth-sso', git: 'git@bitbucket.org:sputniks/omniauth-sso.git'

gem 'whenever', require: false

gem 'sentry-raven'
gem 'intercom', '~> 3.9.5'

gem 'ddtrace'

gem 'bootsnap', '>= 1.1.0', require: false

gem 'aasm'
gem 'strong_migrations'
gem 'spyke'
gem 'draper'
gem 'kaminari'

# monitoring
gem "yabeda-rails"
gem "yabeda-sidekiq"#, "~> 0.1.1"
gem "yabeda-prometheus"
gem "yabeda-puma-plugin"
gem "prometheus-client"

gem 'lograge'
gem 'lograge-sql'
gem 'lograge-spyke', git: 'git@bitbucket.org:sputniks/lograge-spyke.git'
gem 'sidekiq-logstash'

group :development, :test do
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  gem 'rspec-rails'
  gem 'factory_bot_rails'
  gem 'faker'
  gem 'webmock'
end

group :development do
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'

  # Deploy
  gem 'capistrano', '~> 3.6'
  gem 'capistrano-rails', '~> 1.2'
  gem 'capistrano3-puma'
  gem 'capistrano-bundler'
  gem 'capistrano-rbenv'
  gem 'capistrano-sidekiq'
end

gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
